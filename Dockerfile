# Export application as static site
FROM node:14 AS build-stage
WORKDIR /app
COPY . .
RUN npm i
RUN npm run export

# Copy exported page
FROM nginx:alpine as runtime
COPY --from=build-stage /app/__sapper__/export /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]